﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparecerMenuMuerte : MonoBehaviour
{
    public GameObject hand;

    // Update is called once per frame
    void Update()
    {
        if (Persistencia.instancia.data.mVidas < 1)
        {
            hand.SetActive(true);
        }
        else
        {

            hand.SetActive(false);
        }
    }

    public void MenuMuerte()
    {
        //si mira la publicidad sus vidas vuelven a ser 3 y se cierra el menu y puede seguir jugando
        Persistencia.instancia.data.mVidas = 3;
        hand.SetActive(false);
        //disparar publicidad > redirigir a mi instagram
        //hacer todo el menu y pausar el juego
    }
}
