﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollBar : MonoBehaviour
{
    public GameObject explosion;
    Slider bar;
    public static float distanceScrollBar;
    public static bool turbo;
    public Rigidbody sphereRB;
    public Transform player;
    void Start()
    {
        bar = GetComponent<Slider>();
        //la scrollbar comienza en 1
        distanceScrollBar = 0;

    }

    void Update()
    {
        explosion.transform.SetParent(player);
        bar.value = distanceScrollBar;

        if (distanceScrollBar == 0)
        {
            turbo = false;
        }
        if (distanceScrollBar == 1 && turbo)
        {
            Debug.Log("estas tirando turbo");
            distanceScrollBar = 0;

            MostrarExplosion();
            sphereRB.AddForce(-transform.right *15 * 5, ForceMode.Impulse);

        }
    }

    public void TocarTurbo(bool pturbo)
    {
        pturbo = true;
        turbo = pturbo;
    }

    private void MostrarExplosion()
    {
       
        GameObject particulas = Instantiate(explosion, sphereRB.position, Quaternion.identity) as GameObject;
      
        Destroy(particulas,3 );
        
        //Instantiate(sGolpe);
    }
}
