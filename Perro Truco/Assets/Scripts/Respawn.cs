﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Respawn : MonoBehaviour
{
    

    void Update()
    {
        GetInput();
        
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            //reanuda el tiempo
            Time.timeScale = 1;
            //carga la escena desde 0 para que el juego vuelva a comenzar
            SceneManager.LoadScene(0);
        }
    }

    public void cargaNivel(int pNombreNivel)
    {
        SceneManager.LoadScene(pNombreNivel);
      
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (SceneManager.GetSceneByName("Pista1")==SceneManager.GetActiveScene())
            {
                SceneManager.LoadScene(1);
            }
            else
            {
                SceneManager.LoadScene(2);
            }
            
        }
    }

}
