﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioDeMaterial : MonoBehaviour
{
    public Material normal;
    public Material Desaparicion;
    Renderer render;

    void Start()
    {
        render = GetComponent<Renderer>();
        render.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (PRUEBAMovimiento.dadoVuelta)
        {
            render.sharedMaterial = Desaparicion;
        }
        else
        {
            render.sharedMaterial = normal;
        }
    }
}
