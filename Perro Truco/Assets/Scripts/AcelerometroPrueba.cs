﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcelerometroPrueba : MonoBehaviour
{
    public float speed;

    //public float PosicionInicialEnY = 0f;
    //public float PosicionDelGyroEnY = 0f;
    //public float CalibrarEnLaPosicionY = 0f;
    //public GameObject skate;
    //public bool seInicioElJuego;

    void Start()
    {
        //Input.gyro.enabled = true;
        //PosicionInicialEnY = skate.transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = Vector3.zero;
        dir.x = Input.acceleration.y;
        dir.z = Input.acceleration.x;
        if (dir.sqrMagnitude > 0.3)
        {
            dir.Normalize();

            dir *= Time.deltaTime;
            transform.Translate(dir * speed, Space.World);
        }





        //AplicarRotacionDelGiroscopio();
        //AplicarCalibracion();

        //if (seInicioElJuego)
        //{
        //    Invoke("CalibrarEnPosicionY", 3f);
        //    seInicioElJuego = false;
        //}
    }

    //public void AplicarRotacionDelGiroscopio()
    //{
    //    skate.transform.rotation = Input.gyro.attitude;
    //    skate.transform.Rotate(0f, 0f, 180f,Space.Self);
    //    skate.transform.Rotate(90f, 180f, 0f, Space.World);
    //    PosicionDelGyroEnY = skate.transform.eulerAngles.y;
    //}

    //void CalibrarEnPosicionY()
    //{
    //    CalibrarEnLaPosicionY = PosicionDelGyroEnY - PosicionInicialEnY;
    //}

    //void AplicarCalibracion()
    //{
    //    skate.transform.Rotate(0f, -CalibrarEnLaPosicionY, 0f, Space.World);

    //}
}
