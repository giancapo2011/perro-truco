﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public Rigidbody Player;
    public GameObject[] Maps;
    private GameObject instantiator;
    public float PosX;
    public float posY;
    //public Vector3 posAleatoria;
    public static bool destroyMap;
    void Update()
    {
        int mapaRandom = Random.RandomRange(0, 5);

        //posAleatoria = new Vector3(PosX , -14, 38.5f);
        if (Player.transform.position.x < PosX + 650)
        {

            instantiator = Instantiate(Maps[mapaRandom], new Vector3(PosX - 200, posY, 38.5f), Quaternion.identity);

            PosX += -340;
            posY += -0.001f;
            destroyMap = true;
        }
        else
        {
            destroyMap = false;
        }

    }
}
