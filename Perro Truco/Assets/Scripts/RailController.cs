﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailController : MonoBehaviour
{
    public Rigidbody rb;
    public float slideVelocity;
    // Start is called before the first frame update
    void Start()
    {
        rb = GameObject.Find("Skate").GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PRUEBAMovimiento.grindeando)
        {
            rb.freezeRotation = true;
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            //rb.AddForce(new Vector3(-2, 0, 0) * slideVelocity, ForceMode.Force);
            //Physics.gravity = this.transform.position - rb.transform.position;
            //rb.transform.rotation = Quaternion.FromToRotation(transform.forward, -Physics.gravity)/* * transform.rotation*/;
        }
        else
        {
            rb.freezeRotation = false;

            //Physics.gravity = new Vector3(0, -30, 0);
        }

    }


    //public void EngancharSkate()
    //{
    //    Physics.gravity = this.transform.position - transform.position;
    //}


    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        PRUEBAMovimiento.grindeando = true;
    //    }

        
    //}

    //private void OnCollisionExit(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        PRUEBAMovimiento.grindeando = false;
    //    }


    //}

}
