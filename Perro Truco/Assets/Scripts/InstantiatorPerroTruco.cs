﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class InstantiatorPerroTruco : MonoBehaviour
{
    public Rigidbody perroTruco;
    public Transform disparador;
    public float vel;
    public float cont;
    public Vector3 offset;
    NavMeshAgent agente;
   
    void Start()
    {
        cont = 1;
         agente = GetComponent<NavMeshAgent>();
        
    }


    void Update()
    {
        agente.destination = perroTruco.position - offset;
        agente.updateRotation = false;
        //transform.position = Vector3.MoveTowards(transform.position, perroTruco.transform.position + offset, vel * Time.deltaTime);

        //offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X"), Vector3.up) * offset;

        if (!SkateController.tocandoPisoConLasRuedas)
        {
            cont -= Time.deltaTime;
        }
        else
        {
            cont = 0;
        }
      

        if (cont < -10 )
        {
            Trucos.StopRotate = true;
            perroTruco.transform.position = disparador.transform.position;
            //balaImpulso = Instantiate(perroTruco, disparador.position, Quaternion.identity);
            cont = 1;
            SkateController.velocidadActual -= 100;
            perroTruco.AddForce(new Vector3(0, -25f, 0), ForceMode.VelocityChange);
            perroTruco.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            perroTruco.transform.rotation = disparador.transform.rotation;
        }
    }

    public void cargaNivel(bool pNombreNivel)
    {
        if (pNombreNivel)
        {
            Trucos.StopRotate = true;
            perroTruco.transform.position = disparador.transform.position;
            //balaImpulso = Instantiate(perroTruco, disparador.position, Quaternion.identity);
            cont = 1;
            SkateController.velocidadActual -= 100;

            perroTruco.AddForce(new Vector3(0, -25f, 0), ForceMode.VelocityChange);

            perroTruco.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;

            perroTruco.transform.rotation = disparador.transform.rotation;
        }
    }


}
