﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestarVidas : MonoBehaviour
{
    public GameObject Vida1;
    public GameObject Vida2;
    public GameObject Vida3;

    public float timer;

    public bool creciendo;

    // Start is called before the first frame update
    void Start()
    {
        Vida1 = GameObject.Find("Vida1");
        Vida2 = GameObject.Find("Vida2");
        Vida3 = GameObject.Find("Vida3");
    }

    // Update is called once per frame
    void Update()
    {
        
     
        restarVidas();
    }


    public void restarVidas()
    {
        if (Persistencia.instancia.data.mVidas == 3)
        {
            Vida3.SetActive(true);
            Vida2.SetActive(true);
            Vida1.SetActive(true);
        }
        else if (Persistencia.instancia.data.mVidas == 2)
        {
            Vida3.SetActive(false);
        }
        else if (Persistencia.instancia.data.mVidas == 1)
        {
            //timer = 0;


            Vida3.SetActive(false);
            Vida2.SetActive(false);


            if (creciendo)
            {
                Vida1.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f) * Time.deltaTime;
                timer += Time.deltaTime;
            }
            else
            {
                Vida1.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f) * Time.deltaTime;
                timer -= Time.deltaTime;
            }

            if (timer >= 1.5)
            {
                
                creciendo = false;
            }
            
            if(timer <= 0)
            {
               
               
                creciendo = true;
            }
            

        }
        else if (Persistencia.instancia.data.mVidas < 1)
        {
            Vida3.SetActive(false);
            Vida2.SetActive(false);
            Vida1.SetActive(false);
        }
    }
}
