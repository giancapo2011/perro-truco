﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NewRecord : MonoBehaviour
{

    public TextMeshProUGUI recordtxt;
    //public float record;

    // Start is called before the first frame update
    void Start()
    {
        recordtxt.text = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (Persistencia.instancia.data.record < Trucos.puntajeTotalNumero)
        {
            Persistencia.instancia.data.record = Trucos.puntajeTotalNumero;
            recordtxt.text ="Record: " + Persistencia.instancia.data.record.ToString();
        }
    }
}
