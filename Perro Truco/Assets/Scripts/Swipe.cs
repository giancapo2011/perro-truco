﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour
{
    //public SkateController acelerar;
    public static bool acelerando;
    public static bool simpleTouch;

    private bool swipeLeft, swipeRight, swipeUp, swipeDown, swipeLeftXXL, swipeRightXXL, swipeUpXXL, swipeDownXXL;
    private bool isDraging = false;
    private Vector2 startTouch, swipeDelta;

    public Vector2 SwipeDelta { get { return swipeDelta; } }
    public bool SwipeLeft { get { return swipeLeft; } }
    public bool SwipeRight { get { return swipeRight; } }
    public bool SwipeUp { get { return swipeUp; } }
    public bool SwipeDown { get { return swipeDown; } }
    public static bool tap;
    public static bool onlyTap;

    public GameObject trail;
    //public bool SwipeLeftXXL { get { return swipeLeft; } }
    //public bool SwipeRightXXL { get { return swipeRight; } }
    //public bool SwipeUpXXL { get { return swipeUp; } }
    //public bool SwipeDownXXL { get { return swipeDown; } }

    public static float Timer;
 

    public void Start()
    {
        trail.SetActive(false);
    }

    void Update()
    {
        SwipeControll();
        //touching();
        //acelerometro, mover el celular
        //Input.GetAxisRaw
    }


    public void SwipeControll()
    {


        tap = swipeLeft = swipeRight = swipeUp = swipeDown = false;
        if (Input.GetMouseButton(0))
        {
           
            trail.SetActive(true);
            trail.transform.position = Input.mousePosition;
            Timer += Time.deltaTime;
            

            //para poder probarlo en pc
            if (Input.GetMouseButtonDown(0))
            {

                tap = true;
                isDraging = true;
                startTouch = Input.mousePosition;

            }

        }
        else if (Input.GetMouseButton(0))
        {
            isDraging = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("tocaste durante: " + Timer + "segundos");

            Timer = 0;
            isDraging = false;
            trail.SetActive(false);
            Reset();
        }

        //para que funcione en celular
        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                isDraging = true;
                tap = true;
                startTouch = Input.touches[0].position;

            }
        }

        //calcular la distancia del swipe
        swipeDelta = Vector2.zero;

        if (isDraging)
        {

            if (Input.touches.Length > 0)
            {

                swipeDelta = Input.touches[0].position - startTouch;

            }
            else if (Input.GetMouseButton(0) /*&& Input.GetMouseButtonUp(0)*/)
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }

        }




        // cruzo la deadzone?
        if (swipeDelta.magnitude > 125)
        {
            //que direccion estoy deslizando?
            simpleTouch = false;
            float x = swipeDelta.x;
            float y = swipeDelta.y;
           
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                //izquierda o derecha
                if (x < 0)
                {
                    swipeLeft = true;
                }
                else
                {
                    swipeRight = true;
                }
            }
            else
            {
                //arriba o abajo

                if (y < 0)
                {
                    swipeDown = true;
                }
                else
                {
                    swipeUp = true;
                }

            }

            Reset();
        }
        else if(Input.GetMouseButtonDown(0))
        {
            simpleTouch = true;
            Debug.Log("NO CRUZA LA DEADZONE ES UN SIMPLE TOUCH");
        }
       

    }

    private void Reset()
    {

        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
        onlyTap = false;
    }
}
