﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstaculoTutorial : MonoBehaviour
{
    public AudioClip Boing;
    public AudioSource sonidoLlantas;

    public Rigidbody Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player.AddForce(new Vector3(50000, 0, 0), ForceMode.Impulse);
            sonidoLlantas.PlayOneShot(Boing, 1.0f);

        }
    }
}
