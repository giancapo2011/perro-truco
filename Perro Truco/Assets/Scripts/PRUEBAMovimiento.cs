﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PRUEBAMovimiento : MonoBehaviour
{
    //public Material normal;
    //public Material Desaparicion;
    //Renderer render;

    public float slideVelocity;
    

    public GameObject fuego;

    public static bool grindeando;
    public bool grindeandoConf;

    public float dashSpeed;
    private float tiempoDadoVuelta;
    public static bool regular;
    public static bool fakie;
    public static bool dadoVuelta;
    public int perderVida;

    //private const string HORIZONTAL = "Horizontal";
    //private const string VERTICAL = "Vertical";

    public Rigidbody rb;

    public static bool skateTocando;

    public bool skateTOCANDO2;

    public Transform skate;
    public static bool tocandoPisoConLasRuedas;

    public float rayDistance;

    public float JumpVelocity;
    public static float velocidadActual = 0.0f;
    public float velImpulso;
    public static float velMax = 100;
    public float velMin;
    public float velActual2;
    public Swipe swipeControls;

    public float horizontalInput;
    public float verticalInput;
    private float currentSteerAngle;
    private float breakForce;
    private bool isBreaking;

    //[SerializeField] public float motorForce;
    [SerializeField] public float currentBreakForce;
    [SerializeField] public float maxSteerAngle;

    [SerializeField] public WheelCollider frontLeftWheelCollider;
    [SerializeField] public WheelCollider frontRightWheelCollider;
    [SerializeField] public WheelCollider rearLeftWheelCollider;
    [SerializeField] public WheelCollider rearRighttWheelCollider;

    [SerializeField] public Transform frontLeftWheelTransform;
    [SerializeField] public Transform frontRightWheelTransform;
    [SerializeField] public Transform rearLeftWheelTransform;
    [SerializeField] public Transform rearRighttWheelTransform;



    private void Start()
    {
        //render = GetComponent<Renderer>();
        //render.enabled = true;
      
   


    }

   

    private void FixedUpdate()
    {
        grindeandoConf = grindeando ;
         skateTocando = skateTOCANDO2;
        velActual2 = velocidadActual;


        Dash();
        GetInput();
        HandleMotor();
        HandleSteering();
        UpdateWheels();
        DetectarObjetosEnElPiso();
        skateStance();
        stopWheels();
        Grindeando();
    }

    void Grindeando()
    {
        if (grindeando)
        {
            //float cont = +Time.deltaTime;

            rb.AddForce(new Vector3(-2, 0, 0) * slideVelocity, ForceMode.Force);
            tocandoPisoConLasRuedas = true;
            fuego.SetActive(true);

        }
        else
        {
            
            fuego.SetActive(false);
        }
    }

    void stopWheels()
    {
        if (Input.GetMouseButton(0) && CameraCineMachine.TouchingSK8 && Swipe.Timer > 0.5f && Swipe.simpleTouch && tocandoPisoConLasRuedas && !grindeando)
        {
            frontLeftWheelCollider.brakeTorque = Mathf.Infinity;
            frontRightWheelCollider.brakeTorque = Mathf.Infinity;
            rearLeftWheelCollider.brakeTorque = Mathf.Infinity;
            rearRighttWheelCollider.brakeTorque = Mathf.Infinity;
        }
        else 
        {
            frontLeftWheelCollider.brakeTorque = 0;
            frontRightWheelCollider.brakeTorque = 0;
            rearLeftWheelCollider.brakeTorque = 0;
            rearRighttWheelCollider.brakeTorque = 0;
        }

    }

    void Dash()
    {
        if (swipeControls.SwipeDown && tocandoPisoConLasRuedas && !CameraCineMachine.TouchingSK8)
        {
            Debug.Log("tocaste");


            velocidadActual += velImpulso;
            if (velocidadActual > velMax)
            {
                velocidadActual = velMax;
            }

        }
        else
        {

            velocidadActual -= velImpulso;
            //if (skateTocando && !tocandoPisoConLasRuedas)
            //{
            //    velocidadActual -= velImpulso * 2f;
            //}
            if (velocidadActual <= 0)
            {
                velocidadActual = 0;
            }
        }

        if (swipeControls.SwipeUp && tocandoPisoConLasRuedas && !CameraCineMachine.TouchingSK8)
        {
            velocidadActual -= velImpulso;
            if (velocidadActual < velMin)
            {
                velocidadActual = velMin;
            }

        }

        rb.AddForce(transform.forward * velocidadActual, ForceMode.VelocityChange);

    }
    public void acelerar()
    {

        frontLeftWheelCollider.motorTorque = velocidadActual * velImpulso;
        frontRightWheelCollider.motorTorque = velocidadActual * velImpulso;
    }

   public void DetectarObjetosEnElPiso()
    {
        Debug.DrawRay(skate.position, skate.TransformDirection(new Vector3(0, -1, 0)) * rayDistance, Color.red);

        RaycastHit hit;

        Debug.DrawRay(skate.position, skate.TransformDirection(new Vector3(0, 1, 0)) * rayDistance, Color.red);

        RaycastHit hit2;

        if (Physics.Raycast(skate.position, skate.TransformDirection(new Vector3(0, 1, 0)), out hit2, rayDistance))
        {
            Debug.Log(hit2.transform.name);
            //canJump = true;
            DadoVuelta();
          

        }
        else
        {
            dadoVuelta = false;
            
        }
     
 

            if (Physics.Raycast(skate.position, skate.TransformDirection(new Vector3(0, -1, 0)), out hit, rayDistance))
            {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE TENGA SEGUN CON CADA OBJETO
            Debug.Log(hit.transform.name);
            //canJump = true;
            CanRide();
            
            //if (hit.transform.name == "Rail")
            //{
            //    grindeando = true;
            //}
            //else
            //{
            //    grindeando = false;
            //}

            }
        else
        {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE HACE CUANDO EL RAYCAST NO ESTE APUNTANDO A NADA, ES DECIR ESTA DADO VUELTA
            //canJupm = false;

            tocandoPisoConLasRuedas = false;

            if (grindeando)
            {
                PRUEBAMovimiento.skateTocando = true;
            }
            else
            {
                PRUEBAMovimiento.skateTocando = false;
            }
           
            //canJump = false;
            //Crash();

        }
    }
    //private void OnMouseDrag()
    //{
    //    if (swipeControls.SwipeRight)
    //    {
    //        horizontalInput = 1;
    //    }
     
     
    //    if (swipeControls.SwipeLeft)
    //    {
    //        horizontalInput = -1;
    //    }
    //}

    private void OnMouseUp()
    {
        horizontalInput = 0;


        
    }

    void GetInput()
    {
        //if (swipeControls.SwipeDown && SkateController.tocandoPisoConLasRuedas)
        //{
        //    horizontalInput++;
        //}
        //else if(swipeControls.SwipeUp && SkateController.tocandoPisoConLasRuedas)
        //{
        //    horizontalInput--;
        //}
        //horizontalInput = Input.GetAxis(HORIZONTAL);
        //verticalInput = Input.GetAxis(VERTICAL);
        isBreaking = Input.GetKey(KeyCode.Space);
    }

    void HandleMotor()
    {
        //frontLeftWheelCollider.motorTorque = verticalInput * motorForce ;
        //frontRightWheelCollider.motorTorque = verticalInput * motorForce ;
        //currentBreakForce = isBreaking ? breakForce : 0f;

        //if (isBreaking) { ApplyBreaking(); }
    }

    void ApplyBreaking()
    {
        frontRightWheelCollider.brakeTorque = currentBreakForce;
        frontLeftWheelCollider.brakeTorque = currentBreakForce;
        rearLeftWheelCollider.brakeTorque = currentBreakForce;
        rearRighttWheelCollider.brakeTorque = currentBreakForce;

    }


    void HandleSteering()
    {

        currentSteerAngle = maxSteerAngle * horizontalInput ;
        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }



    void UpdateWheels()
    {
        UpdateSingleWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateSingleWheel(frontRightWheelCollider, frontRightWheelTransform);
        UpdateSingleWheel(rearLeftWheelCollider, rearLeftWheelTransform);
        UpdateSingleWheel(rearRighttWheelCollider, rearRighttWheelTransform);
    }

    void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Vector3 pos;
        Quaternion rot;

        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;

    }

    public void CanRide()
    {
     
     
        tocandoPisoConLasRuedas = true;
        PRUEBATrucos.HaciendoTruco = false;
        tiempoDadoVuelta = 0;
    }


    public void skateStance()
    {
        //if (skate.transform.rotation.y <= 0 && skate.transform.rotation.y >= -180)
        //{
           
        //    fakie = true;
        //    regular = false;
        //    Debug.Log("fakie");
        //}
        //else if(skate.transform.rotation.y >= 0 && skate.transform.rotation.y <= 180)
        //{
        //    fakie = false;
        //    regular = true;
        //    Debug.Log("Regular");
        //}
    }

    public void DadoVuelta()
    {
        //tiempoDadoVuelta += Time.deltaTime;
        dadoVuelta = true;
        //if (tiempoDadoVuelta >= 3 )
        //{
        //    //shader de desaparecer
        //    if (SceneManager.GetSceneByName("Pista1") == SceneManager.GetActiveScene())
        //    {
        //        //perder vida
             
        //        perderVida -= 1;
        //        SceneManager.LoadScene(1);

        //    }
        //    else
        //    {
        //        SceneManager.LoadScene(2);
        //    }
            
        //    tiempoDadoVuelta = 0;
        //}
        
         

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            skateTocando = true;
           
        }

        //if (collision.gameObject.CompareTag("Rail"))
        //{
        //    grindeando = true;
        //}
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            skateTocando = false;
        }

        //if (collision.gameObject.CompareTag("Rail"))
        //{
        //    grindeando = false;
        //}
    }
}
