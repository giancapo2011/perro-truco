﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PRUEBACamara : MonoBehaviour
{

    public Vector3 offsetAire;
    public Vector3 offsetPiso;
    private Transform target;
    public Rigidbody skaterb;
    //public GameObject player;
    [Range(0, 20)] public float lerpValue;

    public GameObject ParticulasDash;


    void Start()
    {
        target = GameObject.Find("Skate").transform;
       

    }
    //Update is called once per frame
    private void FixedUpdate()
    {

        //if (PRUEBAMovimiento.tocandoPisoConLasRuedas)
        //{
        //    transform.SetParent(target);
        //}
        //else
        //{

        //    transform.SetParent(null);
        //    transform.position = Vector3.Lerp(transform.position, target.position + offset, lerpValue);
        //    //transform.position = target.transform.position;
        //    transform.LookAt(target);
        //}
        translation();
        rotation();
        particulas();

    }

    public void translation()
    {
        
      
        if (PRUEBAMovimiento.tocandoPisoConLasRuedas && !PRUEBAMovimiento.grindeando)
        {
            var targetposition = target.TransformPoint(offsetPiso);
            transform.position = Vector3.Lerp(transform.position, targetposition, lerpValue * Time.deltaTime);
        }
        else
        {

            transform.position = Vector3.Lerp(transform.position, target.position + offsetAire , lerpValue * Time.deltaTime);
            transform.LookAt(target);
        }

      


    }

    public void rotation()
    {

            var direction = target.position - transform.position;
            var rotation = Quaternion.LookRotation(direction, Vector3.up);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, lerpValue * Time.deltaTime);
    }

    void particulas()
    {
        if (skaterb.velocity.x < -80)
        {
            ParticulasDash.SetActive(true);
            Debug.Log("FIUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUMBA");

        }
        else
        {
            ParticulasDash.SetActive(false);
        }
      
    }


}
