﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Trucos : MonoBehaviour
{
    public Swipe swipeControls;
    public Transform player;
    private Vector3 desiredPosition;

  

    public float JumpVelocity;
    public Rigidbody skate;

    public static bool StopRotate;

    public GameObject KickFlip;
    public GameObject hellFlip;
    public GameObject popShoveit;
    public GameObject ollie;
    public GameObject nollie;
    public GameObject frontFlip;
    public GameObject backFlip;

    public TextMeshProUGUI puntajeTotal;

    public TextMeshProUGUI puntajePorTiempoEnElAire;

    public TextMeshProUGUI multiplicador;

    public float tiempoEnElAire;

    public float multiplicadorNumero;

    public static float puntajeTotalNumero;

    public float contTimeTrick;

    public float kickFlipTimer;
    public bool kickflipOn;

    public float imposibleTimer;
    public bool imposibleOn;

    public float popTimer;
    public bool popOn;

    //private int puntajeTotalNumeroInt;
    //public static Animator animator;

    private void Start()
    {
        puntajeTotal.text = (puntajeTotalNumero).ToString();
        puntajePorTiempoEnElAire.text = null;
        multiplicador.text = null;
        puntajeTotalNumero = 0;
        multiplicadorNumero = 0;
        tiempoEnElAire = 0;
        nollie.SetActive(false);
        ollie.SetActive(false);
        KickFlip.SetActive(false);
        hellFlip.SetActive(false);
        frontFlip.SetActive(false);
        backFlip.SetActive(false);
        popShoveit.SetActive(false);
        //animator = GetComponent<Animator>();
    }

    void Update()
    {
        //puntajeTotalNumeroInt = (int)puntajeTotalNumero;
        Trucosx();
  

        if (kickflipOn)
        {
            kickFlipTimer += Time.deltaTime;
            if (kickFlipTimer>1.7)
            {
                tiempoEnElAire += 2;
                Debug.Log("DOBLE KICKFLIP");
                puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();
            }

        }
        else
        {
            kickFlipTimer = 0;
        }

        if (imposibleOn)
        {
            imposibleTimer += Time.deltaTime;
            if (imposibleTimer > 1.7)
            {
                tiempoEnElAire += 2;
                Debug.Log("DOBLE imposible");
                puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();
            }

        }
        else
        {
            imposibleTimer = 0;
        }


        if (popOn)
        {
            popTimer += Time.deltaTime;
            if (popTimer > 1.7)
            {
                tiempoEnElAire += 2;
                Debug.Log("DOBLE PopShoveit");
                puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();
            }

        }
        else
        {
            popTimer = 0;
        }

    }

    public void Trucosx()
    {


        if (swipeControls.SwipeLeft && SkateController.canJump == false && CameraCineMachine.TouchingSK8)
        {
            if (ScrollBar.distanceScrollBar < 1)
            {
                ScrollBar.distanceScrollBar += 0.5f;
            }
            //kickflip
            desiredPosition += Vector3.left;
            KickFlip.SetActive(true);

            //KickFlip.SetActive(false);
            hellFlip.SetActive(false);
            frontFlip.SetActive(false);
            backFlip.SetActive(false);
            popShoveit.SetActive(false);

            tiempoEnElAire += 150;
           
            puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();

            popOn = false;
            imposibleOn = false;
            kickflipOn = true;

        }



        if (swipeControls.SwipeRight && SkateController.canJump == false && CameraCineMachine.TouchingSK8)
        {
            if (ScrollBar.distanceScrollBar < 1)
            {
                ScrollBar.distanceScrollBar += 0.5f;
            }
            //HellFlip
            desiredPosition += Vector3.right;
            hellFlip.SetActive(true);

            KickFlip.SetActive(false);
            //hellFlip.SetActive(false);
            frontFlip.SetActive(false);
            backFlip.SetActive(false);
            popShoveit.SetActive(false);

            tiempoEnElAire += 150;

            puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();

            kickflipOn = true;
            popOn = false;
            imposibleOn = false;

        }

        if (swipeControls.SwipeUp && SkateController.canJump == false && CameraCineMachine.TouchingSK8)
        {
            if (ScrollBar.distanceScrollBar < 1)
            {
                ScrollBar.distanceScrollBar += 0.5f;
            }
            //Imposible

            desiredPosition += Vector3.forward;

            KickFlip.SetActive(false);
            hellFlip.SetActive(false);
            frontFlip.SetActive(true);
            backFlip.SetActive(false);
            popShoveit.SetActive(false);

            tiempoEnElAire += 150;

            puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();

            popOn = false;
            imposibleOn = true;
            kickflipOn = false;
        }
        if (swipeControls.SwipeDown && SkateController.canJump == false && CameraCineMachine.TouchingSK8)
        {
            if (ScrollBar.distanceScrollBar < 1)
            {
                ScrollBar.distanceScrollBar += 0.5f;
            }
            //backFlip
            desiredPosition += Vector3.back;

            KickFlip.SetActive(false);
            hellFlip.SetActive(false);
            frontFlip.SetActive(false);
            backFlip.SetActive(true);
            popShoveit.SetActive(false);

            tiempoEnElAire += 150;

            puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();

            kickflipOn = false;
            popOn = false;
            imposibleOn = true;
        }

        if (swipeControls.SwipeUp && SkateController.canJump && CameraCineMachine.TouchingSK8)
        {

            //Nollie
            //animacion
            SkateController.canJump = false;
            //desiredPosition += Vector3.up;
            skate.AddForce(new Vector3(0, JumpVelocity*1.8f, 0), ForceMode.Impulse);
            //skate.velocity = new Vector3(0, JumpVelocity, 0 );

            nollie.SetActive(true);
            ollie.SetActive(false);

            popOn = false;
            imposibleOn = false;
            kickflipOn = false;
        }

        if (swipeControls.SwipeDown && SkateController.canJump && CameraCineMachine.TouchingSK8)
        {
            
            //ollie
            //animacion
            //desiredPosition += Vector3.down;
            SkateController.canJump = false;
            skate.AddForce(new Vector3(0, JumpVelocity*1.8f, 0), ForceMode.Impulse);

            nollie.SetActive(false);
            ollie.SetActive(true);

            popOn = false;
            imposibleOn = false;
            kickflipOn = false;
        }

        //if (swipeControls.SwipeLeft && SkateController.canJump && CameraCineMachine.TouchingSK8 && SkateController.tocandoPisoConLasRuedas)
        //{
        //    if (ScrollBar.distanceScrollBar < 1)
        //    {
        //        ScrollBar.distanceScrollBar += 0.5f;
        //    }
        //    //popsheit

        //    SkateController.canJump = false;
        //    skate.AddForce(new Vector3(0, JumpVelocity*1.8f, 0), ForceMode.Impulse);
        //    desiredPosition += Vector3.up;

        //    KickFlip.SetActive(false);
        //    hellFlip.SetActive(false);
        //    frontFlip.SetActive(false);
        //    backFlip.SetActive(false);
        //    popShoveit.SetActive(true);

        //    tiempoEnElAire += 150;

        //    puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();

        //    kickflipOn = false;
        //    popOn = true;
        //    imposibleOn = false;
        //}


        //if (swipeControls.SwipeRight && SkateController.canJump && CameraCineMachine.TouchingSK8 && SkateController.tocandoPisoConLasRuedas)
        //{
        //    if (ScrollBar.distanceScrollBar < 1)
        //    {
        //        ScrollBar.distanceScrollBar += 0.5f;
        //    }

        //    //popsheit

        //    SkateController.canJump = false;
        //    skate.AddForce(new Vector3(0, JumpVelocity * 1.8f, 0), ForceMode.Impulse);
        //    desiredPosition += Vector3.down;

        //    KickFlip.SetActive(false);
        //    hellFlip.SetActive(false);
        //    frontFlip.SetActive(false);
        //    backFlip.SetActive(false);
        //    popShoveit.SetActive(true);

        //    tiempoEnElAire += 150;

        //    puntajePorTiempoEnElAire.text = tiempoEnElAire.ToString();

        //    kickflipOn = false;
        //    popOn = true;
        //    imposibleOn = false;
        //}

        if (Input.GetMouseButton(0) && CameraCineMachine.TouchingSK8 && Swipe.Timer > 0.5f)
        {
            if (!SkateController.grind)
            {

                if (SkateController.canJump && SkateController.tocandoPisoConLasRuedas)
                {
                    skate.AddForce(new Vector3(0, -5f, 0), ForceMode.Impulse);
                }
                else
                {
                    skate.AddForce(new Vector3(0, -1f, 0), ForceMode.Impulse);
                }
            }
            skate.constraints = RigidbodyConstraints.FreezeRotation;

            desiredPosition = Vector3.zero;

            KickFlip.SetActive(false);
            hellFlip.SetActive(false);
            frontFlip.SetActive(false);
            backFlip.SetActive(false);
            popShoveit.SetActive(false);
            nollie.SetActive(false);
            ollie.SetActive(false);

            kickflipOn = false;
            popOn = false;
            imposibleOn = false;

            //puntajePorTiempoEnElAire.text = null;
        }
        else
        {
            skate.constraints = RigidbodyConstraints.None;
        }


    

       

        //if (SkateController.dadoVuelta && swipeControls.SwipeRight)
        //{

        //}



        //if (Swipe.tap && SkateController.canJupm)
        //{
        //    Debug.Log("tocaste");
        //    skate.AddForce(transform.forward * 40 * Time.deltaTime, ForceMode.Impulse);
        //}


        //if(swipeControls.SwipeDownXXL && SkateController.canJupm)
        //{
        //    desiredPosition += Vector3.left;

        //    skate.velocity = new Vector3(0, JumpVelocity *2, 0 * Time.deltaTime);
        //}


        //arreglar: cuando toca el piso tiene que parar de girar. me gusta que se multipliquen los swipes. solo debe funcionar cuando esta en el aire
        player.transform.Rotate(desiredPosition, 240f * Time.deltaTime, Space.World);

        //cancela la rotacion cuando toca el piso, TAMBIEN TENGO QUE HACER QUE SE CANCELE LA ROTACION CUANDO DEJO APRETADA LA PANTALLA
        if (SkateController.canJump)
        {
            if (SkateController.grind)
            {

                multiplicadorNumero++;
                puntajePorTiempoEnElAire.text = Mathf.Round(multiplicadorNumero).ToString();


                //contTimeTrick = Time.deltaTime;
            }
        
         
            if (SkateController.tocandoPisoConLasRuedas)
            {
                //cae bien
                puntajeTotalNumero += tiempoEnElAire;
                if (SkateController.grind)
                {
                    puntajeTotalNumero += Mathf.Round( multiplicadorNumero /5);
                }
               

                puntajeTotal.text = Mathf.Round(puntajeTotalNumero).ToString();
                //puntajeTotal.text = multiplicadorNumero.ToString();
                tiempoEnElAire = 0;
                puntajePorTiempoEnElAire.text = null;

            }
            else
            if (SkateController.dadoVuelta)
            {
                //cae mal
              
            
                    puntajeTotalNumero *= 0;
             
               
                tiempoEnElAire = 0;
                puntajePorTiempoEnElAire.text = null;
            }





            //player.transform.Rotate(new Vector3(0, 0, 0));
            desiredPosition = Vector3.zero;
            skate.constraints = RigidbodyConstraints.None;

            KickFlip.SetActive(false);
            hellFlip.SetActive(false);
            frontFlip.SetActive(false);
            backFlip.SetActive(false);
            popShoveit.SetActive(false);
            nollie.SetActive(false);
            ollie.SetActive(false);

            kickflipOn = false;
            popOn = false;
            imposibleOn = false;
        }

        //movimiento que termina
        //player.transform.position = Vector3.MoveTowards(player.transform.position, desiredPosition, 3f * Time.deltaTime);


    }
}
