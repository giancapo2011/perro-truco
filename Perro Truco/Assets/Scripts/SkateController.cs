﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkateController : MonoBehaviour
{
    public static float velocidadActual = 0.0f;
    public float velImpulso = 1;
    public float velMax = 55550.0f;
    public float velMin;
    public float JumpForece = 1;
    public float JumpVelocity;

    public float gravity;

    public static bool tocandoPisoConLasRuedas;

    public float rayDistance;

    public float rayDistance1;

    public float rayDistance2;

    public Transform skate;

    public static bool canJump;

    public float velGiroYManual;

    public Swipe swipeControls;

    public Rigidbody sphereRB;

    public static bool grind;

    public bool JumpCheck;

    public bool isOnRampa;

    public float slideVelocity;

    public GameObject fuego;

    public bool pared;

    public static bool dadoVuelta;

    public float tiempoDadoVuelta;

    [SerializeField] LayerMask capaPared;
    //[SerializeField] LayerMask capaSuelo;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
        Acelerar();
        DetectObjectsOnFloor();
        Grind();
        //Girar();
        JumpCheck = canJump;

        sphereRB.AddForce(Vector3.down * Time.deltaTime * 10);
    }



    public void DetectObjectsOnFloor()
    {
        Debug.DrawRay(skate.position, skate.TransformDirection(new Vector3(0, -1, 0)) * rayDistance, Color.red);
        Debug.DrawRay(skate.position, skate.TransformDirection(new Vector3(0, 0, 1)) * rayDistance1, Color.blue);
        Debug.DrawRay(skate.position, skate.TransformDirection(new Vector3(0, 1, 0)) * rayDistance2, Color.green);


        RaycastHit hit2;

        RaycastHit hit;

        RaycastHit hit1;


        if (Physics.Raycast(skate.position, skate.TransformDirection(new Vector3(0, 1, 0)), out hit2, rayDistance2))
        {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE TENGA SEGUN CON CADA OBJETO
            Debug.Log(hit2.transform.tag);
            
            dadoVuelta = true;
            DadoVuelta();

        }
        else
        {
            dadoVuelta = false;
        }

        if (Physics.Raycast(skate.position, skate.TransformDirection(new Vector3(0, 0, 1)), out hit1, rayDistance1, capaPared))
        {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE TENGA SEGUN CON CADA OBJETO
            Debug.Log(hit1.transform.tag);
            
            AdondeMira();

        }
        if (Physics.Raycast(skate.position, skate.TransformDirection(new Vector3(0, -1, 0)), out hit, rayDistance))
        {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE TENGA SEGUN CON CADA OBJETO
            Debug.Log(hit.transform.name);
            //canJump = true;
            CanRide();
        }
        else
        {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE HACE CUANDO EL RAYCAST NO ESTE APUNTANDO A NADA, ES DECIR ESTA DADO VUELTA
            //canJupm = false;

            tocandoPisoConLasRuedas = false;
            //canJump = false;
            //Crash();

        }
    }

    public void DadoVuelta()
    {
        tiempoDadoVuelta += Time.deltaTime;
        if (tiempoDadoVuelta<=0.2)
        {
            sphereRB.AddForce(new Vector3(0, -17, 0), ForceMode.VelocityChange);
        }
        
    }

    public void Acelerar()
    {
        if (swipeControls.SwipeDown && canJump && tocandoPisoConLasRuedas)
        {
            Debug.Log("tocaste");
            //if (pared)
            //{
            //    sphereRB.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
            //}
            

            velocidadActual += velImpulso;
            if (velocidadActual > velMax)
            {
                velocidadActual = velMax;
            }

        }
        else
        {

            velocidadActual -= velImpulso;
            if (canJump && !tocandoPisoConLasRuedas)
            {
                velocidadActual -= velImpulso * 2f;
            }
            if (velocidadActual <= 0)
            {
                velocidadActual = 0;
            }
        }

        if (swipeControls.SwipeUp && canJump && tocandoPisoConLasRuedas)
        {
            velocidadActual -= velImpulso;
            if (velocidadActual < velMin)
            {
                velocidadActual = velMin;
            }

        }

        sphereRB.AddForce(transform.forward * velocidadActual, ForceMode.VelocityChange);

       
    }

    //public void Girar()
    //{
    //    if (Input.GetMouseButton(0) && canJump && tocandoPisoConLasRuedas /*|| Input.touchCount > 0*/)
    //    {
    //        //Vector2 touch = Input.mousePosition;
    //        //Touch touch = Input.GetTouch(0);
    //        Vector2 click = Input.mousePosition;
    //        //Vector2 zonaSegura = Screen.safeArea.
    //        float screenHalfY = Screen.height / 5.5f;
    //        float screenHalfX = Screen.width / 2;
    //        float rotateX = 0;
    //        if (/*touch.position.y < screenHalfY ||*/ click.y < screenHalfY && changeModeGirar.rotationModePc)
    //        {
    //            if (/*touch.position.x > screenHalfX ||*/ click.x > screenHalfX) { rotateX = 1; } else { rotateX = -1; }
    //        }

    //        //transform.Rotate(new Vector3(0, rotateX, 0));
    //        sphereRB.AddForce(transform.right * rotateX / 2, ForceMode.VelocityChange);
    //    }
    //}


    //solo se rote en el aire
    //se gire en el piso dando fuerza hacia los costados


    //private void OnMouseDrag()
    //{
    //    if (CameraCineMachine.TouchingSK8 && swipeControls.SwipeLeft)
    //    {

    //        transform.Rotate(new Vector3(0, 1, 0));

    //    }


    //}

    public void Rotate()
    {
        if (Input.acceleration.x > 0.2 || Input.acceleration.x < -0.2 || Input.acceleration.y > 0.7 || Input.acceleration.y < -0.7   /*&& !canJump && !tocandoPisoConLasRuedas*/)
        {
            if (changeModeGirar.rotationModePc)
            {
                transform.Rotate(new Vector3(Input.acceleration.y, Input.acceleration.x, 0) * Time.deltaTime * velGiroYManual);
            }
            

        }

        if (Input.GetMouseButton(0) && !dadoVuelta/* && !canJump*/ /*&& !tocandoPisoConLasRuedas*/ /*|| Input.touchCount > 0*/)
        {
            //Vector2 touch = Input.mousePosition;
            //Touch touch = Input.GetTouch(0);
            Vector2 click = Input.mousePosition;
            //Vector2 zonaSegura = Screen.safeArea.
            float screenHalfY = Screen.height / 5.5f;
            float screenHalfX = Screen.width / 2;
            float rotateX = 0;
            if (/*touch.position.y < screenHalfY ||*/ click.y < screenHalfY &&!changeModeGirar.rotationModePc)
            {
                if (/*touch.position.x > screenHalfX ||*/ click.x > screenHalfX) { rotateX = 1; /*if (tocandoPisoConLasRuedas) sphereRB.AddForce(transform.right * 0.5f, ForceMode.VelocityChange);*/ } else { rotateX = -1; /*if (tocandoPisoConLasRuedas) sphereRB.AddForce(-transform.right * 0.5f, ForceMode.VelocityChange);*/ }
            }

            transform.Rotate(new Vector3(0, rotateX, 0), Space.Self);
           
        }

        if (Input.acceleration.y > 0.4 /*&& !canJump && !tocandoPisoConLasRuedas*/)
        {
            //EJECUTAR ANIMACION DE MANUAL
        }

        if (Input.acceleration.y < -0.4 /*&& !canJump && !tocandoPisoConLasRuedas*/)
        {
            //EJECUTAR ANIMACION DE NOLLIE MANUAL
        }
        
    }

    public void CanRide()
    {
       
        sphereRB.constraints = RigidbodyConstraints.None;
        Trucos.StopRotate = false;
        tocandoPisoConLasRuedas = true;
        //canJump = true;

        if (isOnRampa)
        {
            Debug.Log("Estas en una maldita rampa");
            sphereRB.AddForce(new Vector3(1, 0, 0) * slideVelocity, ForceMode.Acceleration);

            canJump = true;
            //sphereRB.AddForce(new Vector3(0, 0, 1)*slideVelocity, ForceMode.Force);
        }
        //else
        //{
        //    canJump = true;
        //}


    }

    public void Grind()
    {
        if (grind)
        {
            float cont =+ Time.deltaTime;

            sphereRB.AddForce(new Vector3(-2, 0, 0) * slideVelocity, ForceMode.Force);
            
            fuego.SetActive(true);

          
        }
        else
        {
            fuego.SetActive(false);
        }

       
        //aca va a pasar algo cuando lo descubra
    }


   public void AdondeMira()
    {
        //si mira hacia las paredes se frena 
        if (canJump && tocandoPisoConLasRuedas)
        {
            //pared = true;
            sphereRB.AddForce(new Vector3(0, -5, 0), ForceMode.Impulse);
        }
        ////else
        //{
        //    pared = false;
        //}

    }


    //public void DadoVuelta()
    //{
    //    dadoVuelta = true;
    //}

    private void OnCollisionEnter(Collision collision)
    {
    
        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = true;
            grind = false;
        }

        if (collision.gameObject.CompareTag("Rail"))
        {
            Debug.Log("OUYEAH ESTOY GRINDEANDOO");

            grind = true;
            canJump = true;
        }
        if (collision.gameObject.CompareTag("Meta"))
        {
            Debug.Log("llegaste a la meta");
        }

        if (collision.gameObject.CompareTag("Rampa"))
        {
            isOnRampa = true;
        }

        //if (collision.gameObject.CompareTag("Rampa2"))
        //{
        //    Debug.Log("Estas en una maldita rampa");
        //    sphereRB.AddForce(new Vector3(0, -5, 0), ForceMode.Impulse);

        //    canJump = true;
        //}
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            if (!tocandoPisoConLasRuedas)
            {
                grind = false;
                canJump = false;
            }
           
        }

        if (collision.gameObject.CompareTag("Rail"))
        {
            if (!tocandoPisoConLasRuedas)
            {
                grind = false;
                canJump = false;
            }
        }

        if (collision.gameObject.CompareTag("Rampa"))
        {

            isOnRampa = false;
            //canJump = false;
            if (!tocandoPisoConLasRuedas)
            {
                grind = false;
                canJump = false;
            }

        }

    }

}
