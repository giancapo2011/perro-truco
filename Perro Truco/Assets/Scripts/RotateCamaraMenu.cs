﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamaraMenu : MonoBehaviour
{

    [SerializeField] LayerMask capaPared;
    public float rayDistance;
    public bool credits;
    public Transform skate;
    public float cont;
    public float cont2;
    public GameObject creditoTxt;
    // Start is called before the first frame update
    void Start()
    {
        creditoTxt.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        DetectObjectsOnFloor();

        if (credits)
        {
            transform.Rotate(new Vector3(0f, 0f, 0f));
        }
        else
        {
            transform.Rotate(new Vector3(0f, 5f, 0f));
        }


     
    }


    public void DetectObjectsOnFloor()
    {
        Debug.DrawRay(skate.position, skate.TransformDirection(new Vector3(0, 0, 1)) * rayDistance, Color.red);


        RaycastHit hit;


        if (Physics.Raycast(skate.position, skate.TransformDirection(new Vector3(0, 0, 1)), out hit, rayDistance, capaPared))
        {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE TENGA SEGUN CON CADA OBJETO
            Debug.Log(hit.transform.tag);
            credits = true;
            if (cont2>0)
            {
                creditoTxt.SetActive(true);
            }
        }
        else
        {
            credits = false;
            cont += Time.deltaTime;
            if (cont<0.4)
            {
                rayDistance = 20;
                cont = 0;
                creditoTxt.SetActive(false);
            }
        }

    }

    public void cargaNivel(int creditos)
    {
        rayDistance = creditos;
        
        if (cont2>0)
        {
            cont2 = 0;

        }
        else
        {
            cont2 = 1;
        }
}

    public void salir()
    {
        Application.Quit();
    }

}
