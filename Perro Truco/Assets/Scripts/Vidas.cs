﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Vidas : MonoBehaviour
{
    public static Vidas instance;
    
    public float tiempoDadoVuelta;
    public int vidas;
    
    public GameObject hand;

  
    // Start is called before the first frame update
    void Start()
    {
        if (instance !=null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        hand = GameObject.Find("PanelMuerte");
       
        DontDestroyOnLoad(gameObject);

       
    }

    // Update is called once per frame
    void Update()
    {

        
        PerderVidas();
        
        //if (Persistencia.instancia.data.mVidas < 1)
        //{
        //    hand.SetActive(true);
        //}
        //else
        //{

        //    hand.SetActive(false);
        //}
    }

    public void PerderVidas()
    {
        if (PRUEBAMovimiento.dadoVuelta)
        {
            
            //persistir la cantidad de vidas que le quedan aca
            tiempoDadoVuelta += Time.deltaTime;
            if (tiempoDadoVuelta >= 3)
            {
                if (SceneManager.GetSceneByName("Pista1") == SceneManager.GetActiveScene())
                {
                    //perder vida

                    Persistencia.instancia.data.mVidas -= 1;

                    SceneManager.LoadScene(1);

                }
                else
                {
                    SceneManager.LoadScene(2);
                }

                tiempoDadoVuelta = 0;
            }

        }
    }

   
}
