﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class prueba : MonoBehaviour
{

    public static float velocidadActual = 0.0f;
    public float velImpulso = 1;
    public float velMax = 55550.0f;
    public float JumpForece = 1;
    public float JumpVelocity;

    public Rigidbody sphereRB;




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("tocaste");
            velocidadActual += velImpulso;
            if (velocidadActual > velMax)
            {
                velocidadActual = velMax;
            }

        }
        else
        {
            velocidadActual -= velImpulso;
            if (velocidadActual <= 0)
            {
                velocidadActual = 0;
            }
        }

        sphereRB.AddForce(transform.forward * velocidadActual, ForceMode.Impulse);
    }
}
