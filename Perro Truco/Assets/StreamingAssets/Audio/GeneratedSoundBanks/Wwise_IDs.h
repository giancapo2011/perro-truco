/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID PLAY_MX = 2447410425U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MX
        {
            static const AkUniqueID GROUP = 1685527054U;

            namespace STATE
            {
                static const AkUniqueID H1 = 1769415268U;
                static const AkUniqueID H2 = 1769415271U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MX

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
