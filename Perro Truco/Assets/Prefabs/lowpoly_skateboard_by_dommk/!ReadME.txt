---Lowpoly Skateboard - 3D-Model---


\\This model is made by Dominic Kett (Dommk / BlackSonicShadow)//


It's free to use, but keep in mind that if you use it in any kind (games, render, films,...) you
shall credit the author (in this case myself).
The textures for the model are included and should be easily accessable by any type of program.
I've added 2k and 4k resolution textures. Also there is a 2k res texture without the decals and
text on the wheels, in case it bothers something.

Note that this model has an untriangulated poly count which is: 2,434 polygons.
The triangulation pushes the poly count up to ~4.8k polygons.

The textures have been created from the tris-poly model.

>File formats: OBJ, MTL, FBX, STL, DAE, JPG, PNG
>If you need more file formats, feel free to ask me on my cgtrader profile: https://www.cgtrader.com/dommk



Have fun using this model :3